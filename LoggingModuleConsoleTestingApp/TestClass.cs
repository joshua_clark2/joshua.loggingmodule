﻿using System;
using LoggingModule;

namespace LoggingModuleConsoleTestingApp
{
    class TestClass
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("DebugAppender");
        private static LogWriter testLogWriter = new LogWriter();

        static void Main(string[] args)
        {
            // Test logging with log4net within the console test application
            log.Info("************************* Welcome to log4net logging test console application *************************");
            // Call LogWrite function from the logging module to make sure log4net is working inside the class library.
            testLogWriter.LogWrite("<<<<<<<<<< testing log4net in the class library >>>>>>>>>>");
            // Maintain a console window until user decides to stop program
            Console.WriteLine("Hit enter to stop test program");
            Console.ReadLine();
        }
    }
}
