﻿using System;

namespace LoggingModule
{
    public class LogWriter
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger("DebugAppender");

        public LogWriter()
        {
            // empty constructor
        }

        public void LogWrite(string logMessage)
        {
            log.Info(logMessage);
        }
    }
}
